import React, {Component} from "react";
import BookList from "../BookList";
import ShoppingCartTable from "../ShopingCardTable";


export default class HomePage extends Component{

    render() {

        return (
            <div>
                <BookList/>
                <ShoppingCartTable/>


            </div>
        )
    }
}