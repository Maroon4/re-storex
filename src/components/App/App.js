import React, {Component} from 'react'
import {Route, Switch} from 'react-router-dom'

import Spinner from "../Spinner";
import ErrorIndicator from "../ErrorIndicator";
import {HomePage, Orders} from "../Pages";
import ShopHeader from "../ShopHeader";
import BookList from "../BookList/BookList";



const App = () => {


    return (
        <main role="main" className="container">
            <ShopHeader numItems={5} total={220}/>
            <Switch>
                <Route
                    path="/"
                    component={HomePage}
                    exact
                />
                <Route
                    path="/cart"
                    component={Orders}
                />
            </Switch>

        </main>

    )
};

export default App;