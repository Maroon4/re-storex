import React, {Component} from "react";
import BookListItem from "../BookListitem";
import { connect } from 'react-redux';

import withBookstoreService from "../Hoc";
import {fetchBooks} from "../../actions";
import { compose } from "../../utils";
import Spinner from "../Spinner";
import ErrorIndicator from "../ErrorIndicator";
import './bookList.css'


const BookList = ({books}) => {

    return (

        <ul className="book-list">
            {
                books.map((book) => {
                    return (
                        <li key={book.id}><BookListItem book={book}/></li>
                    );
                })
            }
        </ul>
    );
};

class BookListContainer extends Component{

    componentDidMount() {

        this.props.fetchBooks();
        // const bo = bookstoreService.getBooks();
        //
        // console.log(bo);
    }

    render() {

        const {books, loading, error} = this.props;

        console.log(books, null, 3);

        if (loading) {
            return  Spinner;
        }

        if (error) {
            return (
                <ErrorIndicator/>
            )
        }

        return <BookList books={books}/>


    }
}



const mapStateToProps = ({ books, loading, error}) => {
    // console.log(JSON.stringify(books, null, 3));

    return {books, loading, error};



};


const mapDispatchToProps = (dispatch, ownProps) => {
    const {bookstoreService} = ownProps;

    return {
        fetchBooks: fetchBooks(bookstoreService, dispatch)

    }
};

export default compose(
        withBookstoreService(),
        connect(mapStateToProps, mapDispatchToProps)
        )(BookListContainer);


