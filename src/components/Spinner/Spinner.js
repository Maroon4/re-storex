import React from 'react'
import './styles.css'

const  Spinner = () => {
    return (
        <div className="loadingio-spinner-double-ring-g1exn09jzj5">
            <div className="ldio-tqfkkmhnnvj">
                <div></div>
                <div></div>
                <div>
                    <div></div>
                </div>
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    );
};

export  default Spinner;
