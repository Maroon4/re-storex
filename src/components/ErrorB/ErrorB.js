import React, {Component} from "react";
import ErrorIndicator from "../ErrorIndicator";

export default class ErrorB extends Component{

    state = {
        hasError: false
    };

    componentDidCatch() {
      this.setState({
          hasError: true
      })
    }

    render() {
        if (this.state.hasError) {
            return(
                <div>
                    <ErrorIndicator/>
                </div>
            )
        }

        return this.props.children;

    }
}