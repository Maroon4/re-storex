
const booksReguested = () => {

    return {
        type: 'FETCH_BOOKS_REQUEST',
    }
};

const booksLoaded = (newBooks) => {

    return {
        type: 'FETCH_BOOKS_LOAD',
        payload: newBooks
    }
};

const booksError = (error) => {

    return {
        type: 'FETCH_BOOKS_ERROR',
        payload: error
    }
};

const fetchBooks = (bookstoreService, dispatch) => () => {
    dispatch(booksReguested());
    bookstoreService.getBooks()
        .then((data) => dispatch(booksLoaded(data)))
        .catch((err) => dispatch(booksError(err)));
};

export {
    fetchBooks
}