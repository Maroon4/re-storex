import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom'

import App from "./components/App";
import ErrorB from "./components/ErrorB";
import store from "./store";
import BookStore from "./servises/Servises";
import {BookstoreProvider} from "./components/BookstoreServiceContext";


const bookstoreService = new BookStore();

ReactDOM.render(
    <Provider store={store}>
        <ErrorB>
            <BookstoreProvider value={bookstoreService}>
                <Router>
                    <App/>
                </Router>
            </BookstoreProvider>
        </ErrorB>
    </Provider>,
    document.getElementById('root'));
