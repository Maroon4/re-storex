
export default class BookStore  {

    data = [

        {
            id:1,
            title: 'Наполеон Хілл - Закон успіху',
            author: 'Susan J. Fowler',
            price: 32,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/41yJ75gpV-L._SX381_BO1,204,203,200_.jpg'
        },
        {
            id:2,
            title: 'Наполеон Хілл - Думай і багатій',
            author: 'Michael T. Nygard',
            price: 45,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/414CRjLjwgL._SX403_BO1,204,203,200_.jpg'
        },
        {
            id:3,
            title: 'Дейл Карнегі - Як здобувати друзів і здійснювати вплив на людей',
            author: 'Michael T. Nygard',
            price: 45,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/414CRjLjwgL._SX403_BO1,204,203,200_.jpg'
        }

    ];
    getBooks() {

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (Math.random() > 0.75) {
                    reject(new Error('Boolsheet'))
                } else {
                    resolve(this.data);
                }


            }, 700);
        });
    }
}