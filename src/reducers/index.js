
const initialState = {
    books: [],
    loading: true,
    error: null,
    cardItems: [
        {
            id: 1,
            name: 'Book 1',
            count: 3,
            total: 210
        },
        {
            id: 2,
            name: 'Book 2',
            count: 1,
            total: 50
        }
    ],
    orderTotal: 2
};

const reducer = (state = initialState, action) => {


    switch (action.type) {
        case 'FETCH_BOOKS_REQUEST':
               return {
                   ...state,
                   books: [],
                   loading: true,
                   error: null
               };
        case  'FETCH_BOOKS_LOAD':
            return {
                ...state,
                books: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_BOOKS_ERROR': {
            return {
                ...state,
                books: [],
                loading: false,
                error: action.payload
            }
        }

        default:
            return state;
    }


};

export default reducer;